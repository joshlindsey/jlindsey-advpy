This repository is a submission folder for CSCI 310.
This repo contains two folders pertainting to diferent class projects.

The first folder, 'hangman', contains the script and corresponding files to run the game. 
In this folder is a 'README.txt' file that gives an in depth description how to play the game and how it operates.

The second folder, 'kattis', contains different problem submissions. This folder contains 6 prolblems. 
Each problem is provided with the python script and screenshots of the successful submission.
For an in depth descriton, refer to the 'README.md' in this folder.