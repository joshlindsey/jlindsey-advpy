This folder contains the 6 problems I successfully submitted to Kattis.
Each folder contains the script and screenshots corresponding to each problem.

The two problems I provided testing for were 'santa klas' and 'natrij'.
The difficulty of problems breaks down as follows:

1.8 Egypt,
1.9 Das Blinkenlights,
2.2 Simple Addition,
2.5 Black Friday,
2.7 Santa Klas,
2.8 Natrij.

Based on the criteria of the syllabus for this project I would give myself an A for this project 
because I completed the problems required for 90% and wrote two successful test functions.