Create README.md file; describe your project; provide instructions on how to run and play your game.

My hangman game begins by asking the user if they would like to play or not. To play the user must enter the 
character 'y' and if they choose not to play they can enter any other key.

The game then generates a random word the user and lets them know how many characters are in the word and 
how many guesses will be allotted for each word.

The user if asked to enter there first, follwed by the 'enter' key.
If there guess is correct, there guess will go into an array to show the placement of the character.
If the user's guess is incorrect, the user will be told that it is a wrond guess and the character will go
into an array so the user knows what letters have been tried previously.
If the user's guess is not a letter, the game will let them know that an incorecct key has been entered.
After one guess, correct or incoreect, that turn is over and the process repeats


The user is reminded each turn of the number of guesses remaining.
Once an incorrect guess as been inputed, that character and the remaining wrong guess will remain displayed
to remainder the user of characters previously used. 
Similarily, the empty array that the user is adding correct guesses will remain in the console for each turn.

The game ends in one of two ways. First, if the user correctly guesses the word or secondly, if the user
runs out of available guesses. After each, the user is prompted if they want to play again. Entering 'y' 
will begin a new game with a new word. Entering 'n' will exit the game.
