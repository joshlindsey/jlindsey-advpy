# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 16:25:50 2018

@author: joshu
"""

#imports random and string for word generator and ascii characters
import random
import string

#Function check if char is in word, if it occurs in multilpe locations, and,
#returns the locations of the character.
def positions_of_char(word_searching, char_looking_for):
    #Starting location and empty array for locations of char.
    start_at = -1
    locations = []
    
    #While loop to check each index for character
    while True:
        try:
            location = word_searching.index(char_looking_for,start_at+1)
        
        #Statement to break if element is not char of empty.
        except ValueError:
            break
        
        #Adds index of char to locations array
        else:
            locations.append(location)
            start_at = location
    return(locations)

#Main function to check if users guess is in the word.
def main(word1): 
    #Local Variables and empty arrays.
    w1l = len(word1)
    guess_num = 0  
    right_guess = [' ']*w1l
    wrong_guess = []
    
    #Print statement to tell user the number of characters of the random word,
    #and the number of guesses allotted for the word.
    print ("Your word is ", w1l , " charcters. You have", w1l *2 , "guesses.")

    #While loop to itterate of the number of guesses remaining.
    while guess_num < (w1l * 2):
        #Local variables.
        letters = list(string.ascii_lowercase) 
        
        #Asks user for a new guess, adds one to the number of guesses used,
        #and subtracts one from guessess used. Tells user guesses remaining.
        guess_input = input("Guess a letter, then press \'Enter\': ")
        guess_num += 1
        guess_left = w1l*2 - guess_num
        print('You have', guess_left, ' guesses remaining.')
        
        #If statement to check if input is a letter
        if guess_input in letters:
            guess_input = str(guess_input)
            list_of_indicies = positions_of_char(word1, guess_input)
                        
            #Calls function to check for duplicates and prints to new word
            if guess_input in word1:
                for i in list_of_indicies:
                    right_guess[i] = guess_input   
                print("Your word: " + str(right_guess))

     
            #If statement to add a bad guess to the wrong-guess list.
            if guess_input not in word1:
                wrong_guess.append(guess_input)
                print('Incorrect guesses: ', wrong_guess)
                print("Your word: " + str(right_guess))
            
            #The following line of code converts the word from an array to a string
            str1 = ''.join(right_guess)
            
            #If right guesses is equal to random word generated it prompts a win!
            if str1 == word1:
                print('Congradulations you WIN!')   
                
            #If the user has ran out of guesses and not guessed the word it 
            #tells them they ran out of guesses and what the word is.
            if (guess_num == w1l * 2 and word1 != str1):
                print('You have used all of your guesses')
                print('The word you were looking for is', word1,'.')

            #This if statement runs when the user is out guesses or has correctly
            #guesses the word.
            #If user inputs yes to new game it breaks restarts main while loop.
            #if user enters anything other then y it breaks main while loop
            if (guess_num == w1l * 2 or word1 == str1):
                new_game = input('If you want to play again, enter \'y\', if not press any key: ')
                if new_game == 'y':
                    game = 'y'
                    break
                
                else:
                    game = ''
                    break
        
        #Objection to first if statement to check guess for char in alphabet.
        #The purpose: If is 'enter' then grammer of ouput unclear and an input 
        #of 'enter' on last guess will throw an error since 'game' is not defined.
        else:
            #If statements to interpret users invalid input
            #If user presses enter repeatedly, the same set of if statements occur to exit game.
            #If user inputs enter on last guess, exit, else continue.
            if guess_input == '':
                print('Your input of, \'enter\' is in an invalid option.')
                if guess_num == w1l*2:
                    new_game = input('If you want to play again, enter \'y\', if not enter any key: ')
                    if new_game == 'y':
                        game = 'y'
                        break
                    else:
                        game = ''
                        break
                else:
                    continue
                
            else:    
                print('Your input of, ', guess_input, "is not an in the Alphabet.") 
                game=''
    
    #Returns the value 'y' to play again, empty or random to exit game.
    #Return goes to while loop below
    return(game)    

############################################################## 
######################## Start Here ##########################
##############################################################
  
print("Welcome to the hangman game!")
Start_Stop = input('If you want to play, enter \'y\', if not enter any key: ')    

#Block of code generates empty array for word bank, opens file, appends each
#word in text file to word bank while remove the \n at the end of each line,
#closes file and generates random word.
word_bank=[]
file = open("Text_files.txt","r") 
for f in file:
    newstr = f.replace("\n", "")
    word_bank.append(newstr)
file.close()  
new_word = random.choice(word_bank)

#While loop that allows user to play multiple games
while Start_Stop == 'y':
    #This if statement checks to make sure that there is a word to choose from.
    if not word_bank:
        print('You have used all 20 words, thanks for playing')
        break
    
    #This block of code removes the random word from the word-bank so it is not choosen twice.
    new_word = random.choice(word_bank)
    word_bank.remove(new_word)

    ####################################################
    ############ This runs the whole game ###############
    Start_Stop = main(new_word) 

#Exits game.        
else:
    print('Thanks for playing')        